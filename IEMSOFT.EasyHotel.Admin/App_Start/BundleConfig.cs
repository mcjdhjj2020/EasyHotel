﻿using System.Web;
using System.Web.Optimization;

namespace IEMSOFT.EasyHotel.Admin
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/site").Include(
                 "~/Scripts/site.js",
                 "~/Scripts/jquery.PrintArea.js"));
            bundles.Add(new StyleBundle("~/Content/css/site").Include(
       "~/Content/css/site.css"));
        }
    }
}