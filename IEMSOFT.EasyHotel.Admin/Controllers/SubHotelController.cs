﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
using IEMSOFT.EasyHotel.Admin.Lib;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class SubHotelController : BaseController
    {
        //
        // GET: /RoomType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            var model = SubHotelBLL.GetSubHotelList(CurrentUser.GroupHotelId);
            return JsonNet(model);
        }

        public ActionResult Option(string defaultText, string defaultValue)
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new SubHotelDAL(db);
                var ret = dal.GetList(CurrentUser.GroupHotelId);
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.SubHotelId.ToString()
                }).ToList();
            }
            if (string.IsNullOrEmpty(defaultText))
            {
                options.Insert(0, new SelectListItem { Text = CommonConsts.PleaseChoose, Value = CommonConsts.Zero });
            }
            else
            {
                options.Insert(0, new SelectListItem { Text = defaultText, Value = defaultValue ?? "", Selected = true });
            }
            return JsonNet(options);
        }

        [HttpPost]
        public ActionResult Add(SubHotelModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new SubHotelDAL(db);
                var existOne = dal.GetOne(CurrentUser.GroupHotelId, model.Name);
                if (existOne != null)
                {
                    ret.Msg.Add("该分店已经存在!");
                }
                else
                {
                    var dalModel = AutoMapper.Mapper.Map<SubHotel>(model);
                    dalModel.GroupHotelId = CurrentUser.GroupHotelId;
                    dal.Add(dalModel);
                }
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult update(SubHotelModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new SubHotelDAL(db);
                var dalModel = AutoMapper.Mapper.Map<SubHotel>(model);
                dalModel.GroupHotelId = CurrentUser.GroupHotelId;
                dal.Save(dalModel);
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Remove([FromJson]List<SubHotelModel> model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                using (var tx = db.DB.GetTransaction())
                {
                    var dal = new SubHotelDAL(db);
                    var userDal = new UserDAL(db);
                    // var dalModel = AutoMapper.Mapper.Map<List<SubHotel>>(model);
                    foreach (var item in model)
                    {
                        dal.Remove(item.SubHotelId);
                        userDal.ClearSubHotelBySubHotelId(CurrentUser.GroupHotelId, item.SubHotelId);
                    }
                    tx.Complete();
                }
            }
            return JsonNet(ret);
        }
    }
}
